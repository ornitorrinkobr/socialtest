angular.module('main.service', [])
    .factory('Main', function(){
        return {
            urlApi: 'http://beta.uonboard.com/json/'
        }
    });

angular.module('auth.service', [])
    .factory('AuthService', function($http, Main, $q) {
        var auth = {};
        return {
            isTokenValid: function(token){
                var deferred = $q.defer();
                $http.get(Main.urlApi + 'checktoken?token='+token)

                .success(function(data, status, headers, config){
                    if(data.erro)
                        deferred.resolve(false);
                    else{
                        deferred.resolve(true);
                    }
                })
                .error(function(data, status, headers, config) {
                    deferred.reject({error: data});
                });

                return deferred.promise;
            },
            isAuthenticated: function(){
                var deferred = $q.defer();
                auth = this.get();
                if(auth && auth.token){
                    this.isTokenValid(auth.token)
                        .then(function(token){
                            if(token)
                                deferred.resolve(true);
                        });
                }
                else
                    deferred.resolve(false);

                return deferred.promise;
            },
            login: function(data) {
                var deferred = $q.defer();
                
                $http.get(Main.urlApi + 'login?'+data)

                .success(function(data, status, headers, config){
                    if(data.erro)
                        deferred.reject({error: data.erro});
                    else{
                        auth = data.data;
                        localStorage.setItem('auth', JSON.stringify(data.data));
                        deferred.resolve(data.data);
                    }
                })
                .error(function(data, status, headers, config) {
                    deferred.reject({error: data});
                });

                return deferred.promise;
            },
            signin: function(data){
                var deferred = $q.defer();
                
                $http.get(Main.urlApi + 'cadastro?'+data)

                .success(function(data, status, headers, config){
                    if(data.erro)
                        deferred.reject({error: data.erro});
                    else{
                        auth.pending = "Perfil pendente de aprovação"
                        deferred.resolve(data.data);
                    }
                })
                .error(function(data, status, headers, config) {
                    deferred.reject({error: data});
                });

                return deferred.promise;
            },
            get: function(){
                if(localStorage.getItem('auth'))
                    return $.parseJSON(localStorage.getItem('auth'));

                return auth;
            }
        }
    });

angular.module('uOnboard.services', [])

    .factory('PlayListService', function($q, $cacheFactory) {

        var playlist = [
            { title: 'Reggae', id: 1 },
            { title: 'Chill', id: 2 },
            { title: 'Dubstep', id: 3 },
            { title: 'Indie', id: 4 },
            { title: 'Rap', id: 5 },
            { title: 'Cowbell', id: 6 }
          ];

        // We use promises to make this api asynchronous. This is clearly not necessary when using in-memory data
        // but it makes this service more flexible and plug-and-play. For example, you can now easily replace this
        // service with a JSON service that gets its data from a remote server without having to changes anything
        // in the modules invoking the data service since the api is already async.

        return {
            findAll: function() {
                var deferred = $q.defer();
                deferred.resolve(playlist);
                return deferred.promise;
            },

            findById: function(id) {
                var deferred = $q.defer();
                var employee = playlist[id - 1];
                deferred.resolve(employee);
                return deferred.promise;
            },

            findByName: function(searchKey) {
                var deferred = $q.defer();
                var results = playlist.filter(function(element) {
                    var title = element.title;
                    return title.toLowerCase().indexOf(searchKey.toLowerCase()) > -1;
                });
                deferred.resolve(results);
                return deferred.promise;
            },
            scrollY: 0
        }

    });

angular.module('user.services', [])

    .factory('UserService', function($q, AuthService, $http, Main) {
      // Might use a resource here that returns a JSON array

      // Some fake testing data
        var posts = [
            {
                id: 1
,                   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 2
,                   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 3
,                   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 4
,                   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 5
,                   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 6
,                   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 7
,                   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            }
        ];
        var posts2 = [
            {
                id: 8
,                   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 9
,                   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 10
                ,   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 11
                ,   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 12
                ,   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 13
                ,   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            },
            {
                id: 14
                ,   owner:{
                        avatar: 'img/avatar.jpg'
                    ,   name: 'Marty McFly'
                    ,   birth: 'November 05, 1955'
                }
                ,   description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item,\
                        the content is from a card-body consisting of an image and paragraph text. The footer\
                        consists of tabs, icons aligned left, within the card-footer.'
                ,   img: 'img/delorean.jpg'
                ,   likes: '1'
                ,   comments: '5'
            }
        ];

        return {
            getTimeline: function(id) {
                var deferred = $q.defer();
                deferred.resolve(posts);
                return deferred.promise;
            },
            getTimeline2: function(id) {
                var deferred = $q.defer();
                deferred.resolve(posts2);
                return deferred.promise;
            },
            getProfile: function(id){
                var deferred = $q.defer();
                var token = AuthService.get().token;
                
                $http.get(Main.urlApi + 'perfil?token='+token+'&id='+id)

                .success(function(data, status, headers, config){
                    if(data.erro)
                        deferred.reject({error: data.erro});
                    else{
                        deferred.resolve(data.data);
                    }
                })
                .error(function(data, status, headers, config) {
                    deferred.reject({error: data});
                });

                return deferred.promise;
            }
        }

});
