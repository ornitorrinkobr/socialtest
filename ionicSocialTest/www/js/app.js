angular.module('uOnboard'
  , [
      'ionic'
    , 'main.service'
    , 'auth.service'
    , 'auth.controllers'
    , 'uOnboard.services'
    , 'uOnboard.controllers'
    , 'user.services'
    , 'user.controllers'
    ])

.run(function($ionicPlatform, $rootScope, AuthService, $state) {
  $ionicPlatform.ready(function() {
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
      AuthService.isAuthenticated()
      .then(function(authenticated){
        if (toState.authenticate && !authenticated){
          $state.transitionTo("login");
          event.preventDefault(); 
        }
      })
    });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    // setup an abstract state for the tabs directive
    .state('menu', {
      url: "",
      abstract: true,
      templateUrl: "templates/menu.html"
    })

    .state('login', {
      url: "/login",
      templateUrl: "templates/login.html",
      controller: 'LoginCtrl'
    })

    .state('singin', {
      url: "/signin",
      templateUrl: "templates/signin.html",
      controller: 'SigninCtrl'
    })

    .state('menu.tab', {
      views: {
        'tab': {
          templateUrl: 'templates/tabs.html',
        }
      }
    })

    .state('menu.tab.profileContainer', {
      abstract: true,
      views: {
        'profileContainer': {
          templateUrl: 'templates/profileContainer.html'
        }
      },
      authenticate: true
    })

    .state('menu.tab.profileContainer.content', {
      url: '/profile/:id',
      views: { 
        'profile': {
          templateUrl: 'templates/_profile.html',
          controller: 'ProfileCtrl'
        },
        'timeline': {
          templateUrl: 'templates/_timeline.html',
          controller: 'TimelineCtrl'
        },
        'profileContainer': {
          templateUrl: 'templates/profileContainer.html',
          controller: 'ProfileCtrl',
        }
      },
      authenticate: true
    })
    
    .state('menu.tab.friends', {
      url: '/friends',
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-friends.html',
          controller: 'FriendsCtrl'
        }
      },
      authenticate: true
    })
    .state('menu.tab.friend-detail', {
      url: '/friend/:friendId',
      views: {
        'tab-friends': {
          templateUrl: 'templates/friend-detail.html',
          controller: 'FriendDetailCtrl'
        }
      },
      authenticate: true
    })

    .state('menu.tab.account', {
      url: '/account',
      views: {
        'tab-account': {
          templateUrl: 'templates/tab-account.html',
          controller: 'AccountCtrl'
        }
      },
      authenticate: true
    })

    $urlRouterProvider.otherwise("/profile/:id");
});


