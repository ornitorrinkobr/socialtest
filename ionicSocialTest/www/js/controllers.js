angular.module('auth.controllers', [])
  .controller('LoginCtrl', function($scope, AuthService, $state) {
    
    var auth = AuthService.get();

    $scope.frm = {
    };

    $scope.frmLogin = {};
    $scope.frmLogin.submitted = false;

    if(auth && auth.pending)
      $scope.frm.error = auth.pending;

    $scope.submit = function() {
      $scope.frmLogin.submitted = true;

      if($scope.frmLogin.$invalid)
        return ;

      AuthService.login($.param($scope.frm))
      .then(function(data){
        console.log(data)
        $state.transitionTo("menu.tab.profile", {id:data.id_perfil});
      },
      function(error){
        $scope.frm.error = error.error;
      });
    };
  })
  .controller('SigninCtrl', function($scope, AuthService, $state) {
    
    $scope.frm = {
    };
    $scope.frmSignin = {};
    $scope.frmSignin.submitted = false;

    $scope.submit = function() {
      $scope.frmSignin.submitted = true;

      if($scope.frmSignin.$invalid)
        return ;

      AuthService.signin($.param($scope.frm))
      .then(function(data){
        console.log(data);
        $state.transitionTo("login");
      },
      function(error){
        $scope.frm.error = error.error;
        console.log('error => ', error);
      });
    };
  })

angular.module('uOnboard.controllers', [])

  .controller('AppCtrl', function($scope) {
  })

  .controller('PlaylistsCtrl', function($scope, PlayListService) {
    	PlayListService.findAll().then(function (playlists) {
  	    $scope.service = PlayListService.scrollY

  	    $scope.playlists = playlists;
  	}); 
  })
  .controller('MenuCtrl', function($scope, $ionicSideMenuDelegate) {
    $scope.toggleLeftSideMenu = function() {
    	console.log($ionicSideMenuDelegate)
      $ionicSideMenuDelegate.toggleLeft();
    };
  })

  .controller('PlaylistCtrl', function($scope, $stateParams) {
  });

angular.module('user.controllers',[])

  .controller('ProfileCtrl', function($scope, UserService, $stateParams) {
    UserService.getProfile($stateParams.id)
      .then(function(data){
        $scope.profile = data
      },
      function(error){
        console.log('error => ', error)
      });
  })

  .controller('TimelineCtrl', function($scope, UserService, $stateParams){
    UserService.getTimeline($stateParams.id).then(function (posts) {
        $scope.items = posts;
    })

    $scope.loadMoreData = function() {
      $('#timeline').append('<div class="row loading-scroll"><i class="icon ion-ios7-reloading icon-refreshing"></i></div>')
      
      UserService.getTimeline2($stateParams.id).then(function (posts) {    
          $scope.items = $scope.items.concat(posts);
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $('.loading-scroll').remove();
      });
    }

    $scope.like = function(post){
      post.likes = parseInt(post.likes) + parseInt(1);
    }
  })

  .controller('FriendsCtrl', function($scope, Friends) {
    $scope.friends = Friends.all();
  })

  .controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
    PlayListService.findAll().then(function (playlists) {
  	    $scope.service = PlayListService.scrollY

  	    $scope.friend = playlists;
  	}); 
  })

  .controller('AccountCtrl', function($scope) {
  });

