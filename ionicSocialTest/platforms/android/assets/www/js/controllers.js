angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope) {
})

.controller('PlaylistsCtrl', function($scope, PlayListService) {
  	PlayListService.findAll().then(function (playlists) {
	    $scope.service = PlayListService.scrollY

	    $scope.playlists = playlists;
	}); 
})
.controller('MenuCtrl', function($scope, $ionicSideMenuDelegate) {
  $scope.toggleLeftSideMenu = function() {
  	console.log($ionicSideMenuDelegate)
    $ionicSideMenuDelegate.toggleLeft();
  };
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});

angular.module('tab.controllers', [])

.controller('DashCtrl', function($scope) {
})

.controller('FriendsCtrl', function($scope, Friends) {
  $scope.friends = Friends.all();
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
  PlayListService.findAll().then(function (playlists) {
	    $scope.service = PlayListService.scrollY

	    $scope.friend = playlists;
	}); 
})

.controller('AccountCtrl', function($scope) {
});

